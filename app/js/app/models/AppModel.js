define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $           = require('jquery'),
        Backbone    = require('backbone'),
        _           = require('underscore');

    // CONTENT :::::::::::::::::::::::::::::::::::
    
    var AppModel = Backbone.Model.extend({

      defaults: {
        region:"",
        sect1_A1:0,
      },
      
      initialize: function(){
        
        console.log(' App model has been initialized.');
        
        // listens for change update  
        this.on('change', function(){
            console.log( 'Model updated');
        });
    }

    });

    return AppModel

});