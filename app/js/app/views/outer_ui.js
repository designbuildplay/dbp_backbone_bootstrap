// THE INTRO VIEW ::::::::::::::::::::::::

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $                  = require('jquery'),
        Backbone           = require('backbone'),
        _                  = require('underscore'),
        projectTemplate    = require("text!../../templates/outer_ui.html"); 

    // CONTENT :::::::::::::::::::::::::::::::::::

    var scope;

    var OuterView = Backbone.View.extend({

        tagName:'div',
        id:"slides",    
        el:'#container',  //selects element rendering to
        template: _.template( projectTemplate ), //selects the template with given name
        
        events: {
             'click #nav': 'restart',
        },


        initialize:function (innerview) {
              scope = this;
              this.render();
              
              console.log("outer Ui rendered")
        },
      
        render:function () {
            
            this.$el.html(this.template()); // or this.$el.empty() if you have no template

            return this;
        },

        restart:function(){
            var Router = require('router');    
            var appRouter = new Router();
            appRouter.navigate('intro', { trigger :true }); 
        },

        // Clean hanging events of the view on change :::::::::::::::::::
        dispose:function(){
            console.log('cleaned')
        }

    });


    // Our module now returns our view
    return OuterView;

});
