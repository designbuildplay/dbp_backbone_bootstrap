define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $           = require('jquery'),
        Backbone    = require('backbone'),
        _           = require('underscore'),
        Project     = require('models.AppModel');

    // CONTENT :::::::::::::::::::::::::::::::::::

    var PrjCollection = Backbone.Collection.extend({
      model:Project,
      url:"/"
    });

    var Projects = new PrjCollection([
      { id: 1,
        sect1_A:0,
        sect2_A:0,
        active:false,
        section:"intro",
      }
    ]); 


    // Return the model for the module
    return PrjCollection, Projects;


});